Repository for the sources of the MINOTAuR core, as well as the experiments
presented in the paper.

Our processors are based on the Ariane, modified by Thales.  It can be fetched
here: https://github.com/thalesgroup/cva6-softcore-contest.git, at commit
0abb1a6d4742dc968083933030d050345fea9d26.  It is also in the branch "thales" of
this repository.

* RISC-V toolchain
The toolchain itself requires about 10 GB of space, building it will take some time.

The toolchain is available at https://github.com/riscv/riscv-gnu-toolchain.
First, clone this repository:

$ git clone --recursive https://github.com/riscv/riscv-gnu-toolchain
$ cd riscv-gnu-toolchain
$ git checkout ed53ae7a71dfc6df1940c2255aea5bf542a9c422

Then, build it:

$ export RISCV=/path/to/install/riscv/compilers
$ ./configure --prefix="$RISCV" --disable-linux --with-cmodel=medany --with-arch=rv32ima
$ make newlib

When this is done, add $RISCV/bin to your path:

$ export PATH=$(realpath "$RISCV"/bin):$PATH

* Questasim
Questa Sim version 10.7g was used to simulate our processors.

* Building the TACLe benchmarks
The TACLe benchmarks are located in the branch "tacle-ariane".

To build them (the RISC-V toolchain must be in your path):

$ git checkout tacle-ariane
$ cd bench
$ make

This step should be fast.  The resulting .mem files will be in
./build/mem/kernel and in ./build/mem/sequential.

* The five processors
** Common steps
For each version of our processors, there is a few common steps to take with
each of them: building coremark, and copying the TACLe benchmarks.

Those steps must be executed at the root of a processor.

*** Building coremark
The RISC-V toolchain must be in your path.

Checkout the processor you want to use, e.g.:

$ git checkout ariane

Then, clone the submodules:

$ git submodule update --init --recursive

Apply the patch "0001-coremark-modification.patch":

$ git apply 0001-coremark-modification.patch

Then build coremark.mem:

$ cd sw/app
$ make coremark.mem
$ cd ../..

*** Copying the TACLe benchmarks
The TACLe benchmarks must have been built for the CVA6.  Copy them to sw/app:

$ cp /path/to/tacle-bench/bench/build/mem/kernel/*.mem sw/app/
$ cp /path/to/tacle-bench/bench/build/mem/sequential/*.mem sw/app

** baseline
This processor is stored in the branch "ariane".

** MINOTAuRβ
This processor is stored in the branch "minotaur_beta".

** MINOTAuR
This processor is stored in the branch "minotaur".

** seqAriane
This processor is stored in the branch "seqariane".

** sicAriane
This processor is stored in the folder "sicariane".

* Running the benchmarks
To run a specific benchmark, run the following command in the root of the
processor:

$ make sim batch-mode=1 APP="$BENCHMARK"

$BENCHMARK is the name of the binary to run.  For instance, if you want to run
"coremark.mem" (located in sw/app), $BENCHMARK will be "coremark".

By default, Ariane will log all executed instructions in a file, so this may
take quite some space (over a few GB for the longest benchmarks).

Example run:

$ make sim batch-mode=1 APP=fac
… lot of boring lines from Questa …
# [TB]                    0 - Asserting hard reset
# [TRACER] Output filename is: trace_hart_0.log
# [JTAG] SoftReset Done(                  80)
# [TB]                 9620 - Halting the Core
# Loading application to memory from /path/to/the/processor//sw/app/fac.mem
# [TB]                21300 - Writing the boot address into dpc
# [TB]                31520 - Resuming the CORE
# [UART]: brpending: 40; mempending: 295; lsu: 0; imiss: 27; imisscnt: 62; total time: 1247; base time: 980; total committed: 274; base committed: 147

Where:

 - brpending is the number of predicted branches;
 - mempending is the number of memory instructions;
 - lsu is the number of inversions on the bus (ie. when a D$ was blocked because
   a I$ request was occurring);
 - imiss is the number of I$ miss happening while there was a memory instruction
   in the pipeline;
 - imisscnt is the number of I$ miss;
 - total time is the number of elapsed cycles at the end of the program;
 - base time is the number of elapsed cycles when main() was called;
 - total committed is the number of instructions that were committed at the end
   of the program;
 - base committed is the number of instructions that were committed when main()
   was called.

After that message, a bell character ('\a') is printed.
